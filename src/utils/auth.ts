import type { ProviderRpcError } from "../types";
import { AuthError } from "../types";
import {
  authError,
  currentAccount,
  currentChain,
  requestAccountsPending,
} from "../stores";

export function connectWallet() {
  // 1 second delay so the 'not-allowed' button styling isn't shown
  // while the MetaMask popup is loading
  setTimeout(() => requestAccountsPending.set(true), 1000);

  ethereum
    .request({ method: "eth_requestAccounts" })
    .then(handleAccountsChanged)
    .catch((err: ProviderRpcError) => {
      console.error({ err });

      if (err.code === 4001) {
        // EIP-1193 userRejectedRequest error:
        // user rejected the connection request
        authError.set(AuthError.NOT_CONNECTED);
      }
    })
    .finally(() => requestAccountsPending.set(false));
}

export function handleAccountsChanged(accounts: string[]) {
  if (!accounts.length) {
    // MetaMask is locked or the user has not connected any accounts
    currentAccount.set(null);
    authError.set(AuthError.NOT_CONNECTED);
    requestAccountsPending.set(false);
    return;
  }

  // clear error in case user previously rejected connection request
  authError.set(null);

  const [account] = accounts;
  currentAccount.set(account);
}

export async function checkChainId() {
  const chainId = await ethereum.request({ method: "eth_chainId" });

  currentChain.set(chainId);
}

export function handleChainChanged() {
  authError.set(AuthError.CHAIN_CHANGED);

  setTimeout(() => window.location.reload(), 1500);
}

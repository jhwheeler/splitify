const formatDistributions = distributions => distributions.map(({ recipient, percentage }) => [recipient, percentage])

module.exports = { formatDistributions }

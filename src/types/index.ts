import type Big from "big.js";

export interface ProviderRpcError extends Error {
  message: string;
  code: number;
  data?: unknown;
}

export interface Recipient {
  address: string;
  amount: Big;
  percent: Big;
}

export enum AuthError {
  NOT_CONNECTED = "Connect to MetaMask to use Omnia",
  CHAIN_CHANGED = "Chain has changed, reloading...",
  INCORRECT_CHAIN = "Please use Mainnet. You can change from your MetaMask wallet.",
}

export enum EthereumChain {
  MAINNET = "0x01",
  ROPSTEN = "0x03",
  RINKEBY = "0x04",
  GOERLI = "0x05",
  KOVAN = "0x02a",
}

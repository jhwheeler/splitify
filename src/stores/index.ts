import { writable } from "svelte/store";
import type { Writable } from "svelte/store";
import type { AuthError } from "../types";

export const currentAccount: Writable<string> = writable(null);
export const currentChain: Writable<string> = writable(null);
export const authError: Writable<AuthError> = writable(null);
export const requestAccountsPending: Writable<boolean> = writable(false);

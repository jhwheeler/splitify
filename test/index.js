const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
const { ethers, waffle } = require('hardhat')

const { assert, expect } = chai
chai.use(chaiAsPromised)

const { provider } = waffle
const [wallet_1, wallet_2] = provider.getWallets()


describe('Divai', () => {

  describe('#commissions', () => {
    let Divai
    let divai

    beforeEach(async () => {
      Divai = await ethers.getContractFactory('Divai')
    })

    it('Should deploy successfully with a commission less than 10000', async () => {
      await expect(Divai.deploy(9999)).to.not.be.reverted
    })

    it('Should fail to deploy with a commission higher than 9,999 (i.e. 99.99%)', async () => {
      await expect(Divai.deploy(10000)).to.be.reverted
    })

    it('Should get the commission set on deployment', async () => {
      divai = await Divai.deploy(1)
      await divai.deployed()
      expect(await divai.getCommission()).to.equal(1)
    })
  })


  describe('#balance', () => {
    let divai

    beforeEach(async () => {
      const Divai = await ethers.getContractFactory('Divai')
      divai = await Divai.deploy(1)
      await divai.deployed()
    })

    it('Should initially have a balance of 0', async () => {
      expect(await divai.getBalance()).to.equal(0)
    })

    it('Should deposit ETH to the contract', async () => {
      await divai.deposit({ from: wallet_1.address, value: 10 })
      expect(await divai.getBalance()).to.equal(10)
    })

    it('Should withdraw balance to owner account', async () => {
      const initialBalance = await provider.getBalance(wallet_1.address)

      const valueToDeposit = ethers.BigNumber.from("1000000000000000000")
      await divai.connect(wallet_2).deposit({ from: wallet_2.address, value: valueToDeposit })

      const contractBalance = await divai.getBalance()

      const { hash, gasPrice } = await divai.withdrawBalance()

      const { gasUsed } = await provider.getTransactionReceipt(hash)

      const gasFee = gasUsed.mul(gasPrice)
      const amountWithdrawn = contractBalance.sub(gasFee)

      const expectedBalance = initialBalance.add(amountWithdrawn)
      const newBalance = await provider.getBalance(wallet_1.address)

      expect(expectedBalance).to.equal(newBalance)
    })

    it('Should reject attempts by non-owner accounts to withdraw balance', async () => {
      await expect(divai.connect(wallet_2).withdrawBalance()).to.be.reverted
    })
  })
})

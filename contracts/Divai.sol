// SPDX-License-Identifier: MIT

pragma solidity ^0.8.1;

/// @title A split payment processor
/// @author Alacritas (Jackson Holiday Wheeler)
/// @notice Split a transaction among multiple recipients with configurable percentage distributions
contract Splitify {
  struct Distribution {
    address recipient;
    uint16 percentage;
  }

  /// @notice Percentage that contract owner receives with every transaction
  /** @dev Must be between 0 and 9999; divide by 100 to get the percentage value.
   *  So 1.01% is represented as `101`; 99% is `9999`.
   */
  uint16 public commission = 0;

  // 10000 is used to allow percentages as small as 0.01%; using 100 would only allow down to 1%
  uint16 constant private PERCENTAGE_DIVISOR = 10000;

  address owner;

  event Transfer(address from, address to, uint amount);

  modifier onlyOwner() {
    require(msg.sender == owner, "Only the owner can execute this function");
    _;
  }

  constructor(uint16 _commission) {
    owner = msg.sender;
    setCommission(_commission);
  }

  function deposit() external payable {}

  /// @notice Set the commission for each transaction
  function setCommission(uint16 _commission) private {
    require(_commission < PERCENTAGE_DIVISOR, "Commission must be less than 10,000 (i.e. 100%)");
    commission = _commission;
  }

  function getCommission() external view returns (uint16) {
    return commission;
  }

  /// @dev Deducts the commission from the transaction and returns the difference as the amount to transfer
  function deductCommission() private returns (uint256) {
    uint fee = msg.value * commission / PERCENTAGE_DIVISOR;
    return msg.value - fee;
  }

  function getBalance() external view returns (uint256) {
    return address(this).balance;
  }

  /// @notice Allows the contract owner to withdraw the balance from the commissions paid out
  function withdrawBalance() external onlyOwner {
    uint balance = address(this).balance;
    require(balance > 0, "No balance to withdraw");
    payable(msg.sender).transfer(balance);
  }

  /// @notice Splits the received ETH and sends to the provided addresses as per the percentages specified
  /** @dev Provide an array of tuples with the recipient addresses and percentages.
   *  Percentages must be between 0 and 10000: a value of `1` is 0.01% and 9999 is 99.99%.
   */
  function splitTransaction(Distribution[] memory distributions) external payable {
    require(msg.value > 0, "You must send an Ether value to be split");

    require(distributions.length > 0,
      "Recipients and their percentages must be in tuple format. For example, `[[\"0x123\",5000],[\"0xabc\",5000]]`"
    );

    uint16 percentagesTotal = 0;
    for (uint i = 0; i < distributions.length; i++) {
      percentagesTotal = percentagesTotal + distributions[i].percentage;
    }

    require(
      // allow 99.99% (for even 3-way splits, for example)
      percentagesTotal == PERCENTAGE_DIVISOR || percentagesTotal == PERCENTAGE_DIVISOR - 1,
      "Percentages must add up to 100%"
    );

    uint256 amountToTransfer = deductCommission();
    for (uint i = 0; i < distributions.length; i++) {
      address recipient = distributions[i].recipient;
      uint16 percentage = distributions[i].percentage;
      uint amount = amountToTransfer * percentage / PERCENTAGE_DIVISOR;

      payable(recipient).transfer(amount);
      emit Transfer(msg.sender, recipient, amount);
    }
  }
}

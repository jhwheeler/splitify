const hre = require('hardhat')

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface. If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled

  // await hre.run('compile')

  const Divai = await hre.ethers.getContractFactory('Divai')
  const divai = await Divai.deploy(10000)

  await divai.deployed()
  console.log('Divai deployed to:', divai.address)

  const commission = await divai.getCommission()
  console.log('Commission:', commission)
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error)
    process.exit(1)
  })
